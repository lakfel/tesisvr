﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishStateHand : StateMachineBehaviour
{


	 //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject gameManager = GameObject.Find("GameManager");
        LvlManager.Platform currentPlatform = gameManager.GetComponent<NetworkManager>().platform;
        if (currentPlatform.Equals(LvlManager.Platform.GearVR) && animator.transform.gameObject.GetComponent<PhotonView>().isMine)
        {
            animator.SetBool("ClosedHand2", true);
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	//OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject gameManager = GameObject.Find("GameManager");
        LvlManager.Platform currentPlatform = gameManager.GetComponent<NetworkManager>().platform;
        if (currentPlatform.Equals(LvlManager.Platform.GearVR) && animator.transform.gameObject.GetComponent<PhotonView>().isMine)
        {
            animator.SetBool("ClosedHand2", false);
            if (stateInfo.IsName("Bas.RightHandPointer") && !animator.GetBool("Walking"))
            { 
                //  animator.SetBool("ClosedHand", false);
                GameObject interactionMannager = animator.transform.root.GetChild(1).gameObject;
                GearInteractionManager gearInteraction = interactionMannager.GetComponent<GearInteractionManager>();
                gearInteraction.manageRightHandIdle();
            }
        }

    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
