﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GearInteractionManager : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the oculus inputs
    public OculusInputManager oculusInputManager;

    //---------------------------------------
    // Switchable Interactions Variables
    //---------------------------------------

    // Enum for the different interaction modes available to the device
    public enum CURRENT_GEAR_MODE { INTERACTION, MOVEMENT, MENU, WALKING, GREETING };

    // Current interaction Mode
    public CURRENT_GEAR_MODE currentMode;
    public CURRENT_GEAR_MODE lastMode;

    // Script that handles the teleportation process for the right arm
    public MotionManager motionManager;

    // Script that handles the interaction process for the left arm
    public PlayerInteraction playerInteractionManager;

    public UtilsPack utilsPackManager;

    //---------------------------------------
    // Private Variables
    //---------------------------------------

    private bool hasTakenItem;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start ()
    {
        if(GameObject.Find("IsTesting") == null)
        {
            string[] nameSplit = transform.parent.gameObject.name.Split(' ');
            string number = nameSplit[1];
            GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject temp in array)
            {
                if (temp.name.Contains("Camera") && temp.name.Contains(number))
                {
                    oculusInputManager = temp.GetComponent<OculusInputManager>();
                    break;
                }
            }
        }      

        if (oculusInputManager != null)
        {
            oculusInputManager.OnCameraMoveEvent += OnCameraMoved;
            oculusInputManager.OnTouchCenterEvent += OnCentralButtonDown;
            oculusInputManager.OnTouchRightEvent += OnSwipeRight;
            oculusInputManager.OnTouchLeftEvent += OnSwipeLeft;
            oculusInputManager.OnTouchUpEvent += OnSwipeUp;
            oculusInputManager.OnTouchDownEvent += OnSwipeDown;
        }
        motionManager.initializeMotionMechanic();
        //OnSwipeRight();
	}


    private void Update()
    {
        if(currentMode.Equals(CURRENT_GEAR_MODE.MOVEMENT) && motionManager.isMoving)
        {
            currentMode = CURRENT_GEAR_MODE.WALKING;

            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "Walking", true);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "ClosedHand2", false);

        }
        if (!motionManager.isMoving && currentMode.Equals(CURRENT_GEAR_MODE.WALKING))
        {
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "Walking", false);
            currentMode = CURRENT_GEAR_MODE.MOVEMENT;
        }

        if(currentMode.Equals(CURRENT_GEAR_MODE.MOVEMENT))
            refreshLabels(0, 0); // RIGTH Movement
        else if (currentMode.Equals(CURRENT_GEAR_MODE.WALKING))
            refreshLabels(0, 4); // RIGHT MOVEMENT
        else if (currentMode.Equals(CURRENT_GEAR_MODE.GREETING))
            refreshLabels(2, 2); // UP GREETING
        else if (currentMode.Equals(CURRENT_GEAR_MODE.MENU))
            refreshLabels(3, 3); // Down MENU
        else if (currentMode.Equals(CURRENT_GEAR_MODE.INTERACTION))
            refreshLabels(1, 1); // LEFT

    }


    /// <summary>
    /// Method used when the Gear user swipes left on the device
    /// </summary>
    public void OnSwipeLeft()
    {
        if (!motionManager.isMoving)
        {
            refreshLabels(1,1); // LEFT INTERACTION
            if (currentMode != CURRENT_GEAR_MODE.INTERACTION)
            {
                currentMode = CURRENT_GEAR_MODE.INTERACTION;
            }
            motionManager.DBHandler.sendDetail("interaction mode", "yes", 0);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "ClosedHand", false);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 0, "LeftUp", true);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 1, "RUP", false);

            // Camera Anims
            motionManager.animationManager.setBoolAnim(false, 0, "LeftUp", true);
            motionManager.animationManager.setBoolAnim(false, 1, "RUP", true);
        
            motionManager.setMarkerStatus(false);
        }
        else
        {
            motionManager.DBHandler.sendDetail("interaction mode", "not, walking", 0);
        }
    }

    public void OnSwipeUp()
    {
        if (!motionManager.isMoving)
        {
            {
                motionManager.DBHandler.sendDetail("greeting", "yes", 0);
            }

            refreshLabels(2,2); // UP GREETING
          
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "ClosedHand", false);
            photonView.RPC("sendAnimationEnaAnim", PhotonTargets.All, true, 3, true);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 3, "IsGreeting", true);

            motionManager.animationManager.setBoolAnim(false, 0, "LeftUp", true);
            motionManager.animationManager.setBoolAnim(false, 1, "RUP", true);
            
        
            currentMode = CURRENT_GEAR_MODE.GREETING;
        }
        else
        {
            motionManager.DBHandler.sendDetail("greeting", "not, walking", 0);
        }
    }

    /// <summary>
    /// Method used when the Gear user swipes up on the device
    /// </summary>
    public void OnSwipeRight()
    {
        if (!motionManager.isMoving)
        {
            refreshLabels(0,0); // RIGTH Movement
            playerInteractionManager.tutorialPress(true, false, 0);
            playerInteractionManager.tutorialPress(true, true, 1);
            if (currentMode != CURRENT_GEAR_MODE.MOVEMENT)
            {
                currentMode = CURRENT_GEAR_MODE.MOVEMENT;
            }
            motionManager.DBHandler.sendDetail("movement mode", "yes", 0);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "ClosedHand", true);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 0, "LeftUp", false);

            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 0, "LeftUp", false);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 1, "RUP", false);
            // Camera Anims
            motionManager.animationManager.setBoolAnim(false, 0, "LeftUp", true);
            motionManager.animationManager.setBoolAnim(false, 1, "RUP", true);
     
            motionManager.setMarkerStatus(true);
        }
        else
        {
            motionManager.DBHandler.sendDetail("movement mode", "not, walking", 0);
        }
    }

    /// <summary>
    /// Method used when the Gear user swipes up on the device
    /// </summary>
    public void OnSwipeDown()
    {
        if (!motionManager.isMoving)
        {
            currentMode = CURRENT_GEAR_MODE.MENU;
            playerInteractionManager.nextUtil();
            refreshLabels(3,3); // Down MENU

            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 0, "ClosedHand", true);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "ClosedHand", false);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 0, "LeftUp", false);
            photonView.RPC("sendAnimationBoolAnim", PhotonTargets.Others, false, 1, "RUP", false);

            motionManager.animationManager.setBoolAnim(false, 0, "LeftUp", true);
            motionManager.animationManager.setBoolAnim(false, 1, "RUP", true);

            motionManager.DBHandler.sendDetail("show menu", "yes", 0);

            if (playerInteractionManager.util.GetComponent<Util>().utilAction.Equals(Util.UTIL_ACTION.HAND))
            {
                photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 0, "ClosedHand", false);
            }
            currentMode = CURRENT_GEAR_MODE.INTERACTION;
        }
        else
        {
            motionManager.DBHandler.sendDetail("show menu", "not, walking", 0);
        }
        
    }
    

    public void refreshLabels(int index, int index2)
    {
        for (int i = 0; i < 5; i++)
        {
            
            playerInteractionManager.tutorialPress(i == index || i == index2, false, i*2);
            playerInteractionManager.tutorialPress(i == index || i == index2, true, i * 2 + 1);
        }
    }


    public void manageRightHandIdle()
    {
        currentMode = CURRENT_GEAR_MODE.INTERACTION;
        photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 1, "ClosedHand", false);
    }

    /// <summary>
    /// Method that happends when the central button of the Gear VR has been touched
    /// </summary>
    public void OnCentralButtonDown()
    {
        if (!motionManager.isMoving)
        {
            refreshLabels(4,4);
            switch (currentMode)
            {
                case CURRENT_GEAR_MODE.MOVEMENT:
                    motionManager.DBHandler.sendDetail("interaction", "yes", 0);
                    motionManager.MotionAction();
                    break;
                case CURRENT_GEAR_MODE.INTERACTION:
                    //playerInteractionManager.GetComponent<PhotonView>().RPC("interaction", PhotonTargets.All);
                    playerInteractionManager.interaction();
                    motionManager.DBHandler.sendDetail("interaction", "yes", 0);
                    refreshLeftHand();
                    break;
                case CURRENT_GEAR_MODE.MENU:
                    motionManager.DBHandler.sendDetail("interaction", "not, menu mode", 0);
                    //CircularMenuItem.MENU_ITEM_ACTION actionCurrent = circularMenuManager.updateMenu();
                    //if (circularMenuManager.shown == false)
                    //    currentMode = lastMode;
                    //if(actionCurrent.Equals(CircularMenuItem.MENU_ITEM_ACTION.GREET))
                    //    motionManager.animationManager.setBoolAnim(true, 3, "IsGreeting", true);
                    break;
            }
        }
        else
        {
            motionManager.DBHandler.sendDetail("interaction", "not, walking", 0);
        }
    }

    /// <summary>
    /// Method used to update the camera position 
    /// </summary>
    /// <param name="playerCamera">The Camera Position</param>
    public void OnCameraMoved(Transform playerCamera)
    {
        motionManager.moveCamera(playerCamera);
    }

    public void refreshLeftHand()
    {
        if (photonView.isMine)
            if (!playerInteractionManager.currentStatus.Equals(PlayerInteraction.LEFT_HAND_STATUS.NONE))
                photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 0, "ClosedHand", true);
            //motionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", true);
            else
                photonView.RPC("sendAnimationBoolAnim", PhotonTargets.All, true, 0, "ClosedHand", false);


    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }

    [PunRPC]
    public void sendAnimationBoolAnim(bool isPlayer, int indexAnc, string attr, bool value)
    {
        motionManager.animationManager.setBoolAnim(isPlayer, indexAnc, attr, value);
    }

    [PunRPC]
    public void sendAnimationEnaAnim(bool isPlayer, int indexAnc, bool value)
    {
        motionManager.animationManager.setActivAnim(isPlayer, indexAnc, value);
    }

}
