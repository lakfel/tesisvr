﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusTouchInteractionManager : Photon.PunBehaviour
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the oculus inputs
    public OculusInputManager oculusInputManager;

    //---------------------------------------
    // Switchable Interactions Variables
    //---------------------------------------

    // Script that handles the teleportation process for the right arm
    public MotionManager motionManager;

    // Script that handles the interaction process for the left arm
    public PlayerInteraction playerInteractionManager;

    //---------------------------------------
    // Private Variables
    //---------------------------------------

    private bool hasTakenItem;

    private bool isAiming;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start ()
    {
        GameObject oculusTouchCamera = GameObject.Find("OculusTouchCamera");
        if (oculusTouchCamera != null)
        {
            oculusInputManager = oculusTouchCamera.GetComponent<OculusInputManager>();
            oculusInputManager.OnCameraMoveEvent += OnCameraMoved;
            oculusInputManager.OnRightGabbrerBegin += OnRightGrabber;
            oculusInputManager.OnLeftGrabberBegin += OnLeftGrabber;
            oculusInputManager.OnRightTriggerBegin += OnRightTriggerPressed;
        }
        motionManager.initializeMotionMechanic();
    }

    public void OnRightGrabber(bool isGrabbing)
    {
        motionManager.animationManager.setBoolAnim(true, 1, "ClosedHand", isGrabbing);
        motionManager.setMarkerStatus(isGrabbing);
        isAiming = isGrabbing;
    }

    public void OnLeftGrabber(bool isGrabbing)
    {
        playerInteractionManager.takeItem(isGrabbing);
        motionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", isGrabbing);
    }

    public void OnRightTriggerPressed()
    {
        if(isAiming == true)
        {
            motionManager.MotionAction();
        }
    }

    /// <summary>
    /// Method used to update the camera position 
    /// </summary>
    /// <param name="playerCamera">The Camera Position</param>
    public void OnCameraMoved(Transform playerCamera)
    {
        motionManager.moveCamera(playerCamera);
    }
}
