﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerInteraction : Photon.PunBehaviour, IPunObservable
{
    public enum LEFT_HAND_STATUS { NONE, HOLD_UTIL, HOLD_KEY, HAND_CLOSE };
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // ID of the actual player
    public int playerID;

    // The local player instance. Use this to know if the local player is represented in the Scene
    public static GameObject LocalPlayerInstance;

    public PlayerColor currentPlayerColor;

    public Transform grabPosition;

    public UtilsPack utilPack;
    //---------------------------------------
    // Protected Variables
    //---------------------------------------

    public GameObject possibleItem;

    protected GameObject possibleDropOffPlace;

    public GameObject possibleUtil;

    protected bool hasTakenItem;

    public GameObject util;

    public TutorialLabelsManager tutorialManager;

    public DBHandler DBHandler;


    public LEFT_HAND_STATUS currentStatus;
    //---------------------------------------
    // Methods
    //---------------------------------------

    void Awake()
    {
        // #Important
        // used in GameManager.cs: we keep track of the localPlayer instance to prevent instantiation when levels are synchronized
        /*
       if (photonView.isMine)
        {
            PlayerInteraction.LocalPlayerInstance = this.gameObject;
        }
        */

    }

    protected void Start()
    {
        currentStatus = LEFT_HAND_STATUS.NONE;
        string nickName = PhotonNetwork.player.NickName.Split(' ')[1];
        int numbre = int.Parse(nickName) + 1;
        //gameObject.name = "Player " + (playerID);
        GameObject island = GameObject.Find("/TutorialIslandP" + numbre + "/Deco/Door/TutorialStatus/TutorialCanvas");
        island.SetActive(true);
        MenuUIHandler menuHandler = island.GetComponent<MenuUIHandler>();
        if (tutorialManager.platform.Equals(LvlManager.Platform.GearVR))
        {
            tutorialManager.labels = menuHandler.modifiableObjectsGear;
        }
        else if (tutorialManager.platform.Equals(LvlManager.Platform.HTCVive))
        {
            tutorialManager.labels = menuHandler.modifiableObjectsVive;
        }
        /*
             if (!photonView.isMine)
            {
                playerAnimator.enabled = false;
                cameraUIHandler.enabled = false;
            }
            */
    }

    public void nextUtil()
    {
        if (currentStatus.Equals(LEFT_HAND_STATUS.HOLD_UTIL) || currentStatus.Equals(LEFT_HAND_STATUS.NONE))
        {
            if (util != null)
            {
                GetComponent<PhotonView>().RPC("PUNHideUtil", PhotonTargets.All);
            }

            GetComponent<PhotonView>().RPC("PUNNextUtil", PhotonTargets.All);

        }

    }

    [PunRPC]
    public void PUNHideUtil()
    {
        util.SetActive(false);
    }

    [PunRPC]
    public void PUNNextUtil()
    {

        util = utilPack.nextUtil();
        if (currentStatus.Equals(LEFT_HAND_STATUS.HOLD_UTIL) || currentStatus.Equals(LEFT_HAND_STATUS.NONE))
        {


            if (util.GetComponent<Util>().utilAction.Equals(Util.UTIL_ACTION.HAND))
                currentStatus = LEFT_HAND_STATUS.NONE;
            else
            {
                currentStatus = LEFT_HAND_STATUS.HOLD_UTIL;
                util.SetActive(true);
                util.transform.parent = grabPosition;
                util.transform.localPosition = Vector3.zero;
                util.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
        }
    }

    private void Update()
    {

        /**if (util != null)
        {
            Util.UTIL_ACTION action = util.GetComponent<Util>().utilAction;
            if (!action.Equals(Util.UTIL_ACTION.HAND))
                refreshUtil();
        }*/
    }

    [PunRPC]
    public void refreshUtil()
    {
        util.SetActive(true);
        setInteractiveItemCollider(false, util);
        //setInteractiveItemAnimatio(false, util);
        util.transform.parent = grabPosition;
        util.transform.localPosition = Vector3.zero;
        util.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }

    public void setInteractiveItemCollider(bool valueP, GameObject interactiveItem)
    {
        SphereCollider sphere = interactiveItem.GetComponent<SphereCollider>();
        sphere.enabled = valueP;
    }

    public void setInteractiveItemAnimatio(bool valueP, GameObject interactiveItem)
    {
        GameObject child = interactiveItem.transform.GetChild(0).gameObject;
        child.GetComponentInChildren<Animator>().SetBool("Moving", valueP);
    }

    [PunRPC]
    public void selectUtilObject(GameObject newUtilObject)
    {
        if (util != null)
            util.SetActive(false);
        util = newUtilObject;
    }

    [PunRPC]
    public void setPlayerInteraction(int playerID)
    {
        setupInfo(playerID);
        // LvlManager.Instance.playersInteractions[playerID] = this;
    }

    public void setupInfo(int pID)
    {
        playerID = pID;

    }

    void OnTriggerEnter(Collider objectColliding)
    {

        if (objectColliding.gameObject.tag == "InteractuableObject")
        {

            InteractiveItem item = objectColliding.gameObject.GetComponent<InteractiveItem>();
            if (possibleItem == null && !item.isGrabed())
            {
                PhotonView objectPhoton = GetComponent<PhotonView>();
                KeyCover cover = objectColliding.gameObject.transform.GetChild(0).GetComponent<KeyCover>();
                if (!cover.destroyed)
                {
                    possibleItem = objectColliding.gameObject;
                }
                else if(item.currentColor == currentPlayerColor.currentColor )
                {
                    possibleItem = objectColliding.gameObject;
                }
            }
        }
        else if (objectColliding.gameObject.tag == "DropoffArea")
        {
            possibleDropOffPlace = objectColliding.gameObject;
        }
        else if (objectColliding.gameObject.tag == "ActionableObjects")
        {
            possibleUtil = objectColliding.gameObject;
        }

    }
    [PunRPC]
    public void interaction()
    {
        if (currentStatus.Equals(LEFT_HAND_STATUS.HAND_CLOSE))
        {
            currentStatus = LEFT_HAND_STATUS.NONE;
        }
        else if (currentStatus.Equals(LEFT_HAND_STATUS.HOLD_KEY))
        {
            releaseObject();
            currentStatus = LEFT_HAND_STATUS.NONE;
        }
        else if (currentStatus.Equals(LEFT_HAND_STATUS.HOLD_UTIL))
        {
            if (util != null)
            {
                PhotonView objectPhoton = GetComponent<PhotonView>();
                objectPhoton.RPC("tryDestroyCover", PhotonTargets.All);
            }
            // tryDestroyCover();
        }
        else if (currentStatus.Equals(LEFT_HAND_STATUS.NONE))
        {
            if (possibleItem != null)
            {
                PhotonView objectPhoton = GetComponent<PhotonView>();
                InteractiveItem item = possibleItem.GetComponent<InteractiveItem>();
                KeyCover cover = possibleItem.transform.GetChild(0).GetComponent<KeyCover>();
                if (item.currentColor == currentPlayerColor.currentColor && cover.destroyed )
                {
                    if (!item.isGrabed())
                    {
                        objectPhoton.RPC("pickObject", PhotonTargets.All);
                        currentStatus = LEFT_HAND_STATUS.HOLD_KEY;
                    }
                    else
                    {
                        possibleItem = null;
                    }
                }
                else
                {
                    currentStatus = LEFT_HAND_STATUS.NONE;
                }
            }
            else if (possibleUtil != null)
                GetComponent<PhotonView>().RPC("pickUtil", PhotonTargets.All);
            //pickUtil();
            else
            {
                currentStatus = LEFT_HAND_STATUS.NONE;
            }
        }
        //if(GetComponent<PhotonView>().isMine)
        //   DBHandler.sendDetail("interaction: " + currentStatus, "yes", 0);
    }
    [PunRPC]
    protected void pickUtil()
    {
        utilPack.pickUtil(possibleUtil);
        possibleUtil = null;
    }

    [PunRPC]
    protected void pickObject()
    {

        setInteractiveItemColliders(false, possibleItem);
        //setInteractiveItemAnimation(false, possibleItem);
        possibleItem.transform.parent = grabPosition;
        possibleItem.GetComponent<SphereCollider>().enabled = false;
        possibleItem.transform.localPosition = Vector3.zero;
        possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);
        possibleItem.transform.GetChild(2).transform.gameObject.SetActive(false);

        InteractiveItem item = possibleItem.gameObject.GetComponent<InteractiveItem>();
        item.setGrabed(true);
    }

    [PunRPC]
    protected void releaseObject()
    {

        if (possibleDropOffPlace != null)
        {
            PuzzleTwoObjects puzzleScript = possibleDropOffPlace.GetComponentInParent<PuzzleTwoObjects>();
            bool answer = puzzleScript.checkPuzzle(possibleItem);

            if (answer == true)
            {
                PhotonView dropOffPhotonView = possibleDropOffPlace.GetComponent<PhotonView>();
                if (PhotonNetwork.connectionState == ConnectionState.Connected && dropOffPhotonView != null)
                {
                    dropOffPhotonView.RPC("solvePuzzle", PhotonTargets.All);
                    photonView.RPC("possibleItemSolved", PhotonTargets.All);
                }
                else
                {
                    puzzleScript.solvePuzzle();
                    possibleItemSolved();
                }
            }
            else
            {
                /* // Notify that is wrong
                 setInteractiveItemColliders(true, possibleItem);
                 //setInteractiveItemAnimation(true, possibleItem);
                 possibleItem.transform.parent = GameObject.Find("InteractiveItems").transform;
                 possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);

                 Vector3 originalHeight = possibleItem.transform.position;
                 originalHeight.y = transform.position.y;
                 possibleItem.transform.position = originalHeight;
                 possibleItem.GetComponent<SphereCollider>().enabled = true;
                 possibleItem.transform.GetChild(2).transform.gameObject.SetActive(true);*/
                GetComponent<PhotonView>().RPC("dropItem", PhotonTargets.All);
            }

        }
        else
        {
            GetComponent<PhotonView>().RPC("dropItem", PhotonTargets.All);
        }
        possibleItem = null;
    }

    [PunRPC]
    public void dropItem()
    {
        setInteractiveItemColliders(true, possibleItem);
        // setInteractiveItemAnimation(true, possibleItem);
        possibleItem.transform.parent = GameObject.Find("InteractiveItems").transform;
        possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);

        Vector3 originalHeight = possibleItem.transform.position;
        originalHeight.y = transform.position.y;
        possibleItem.GetComponent<SphereCollider>().enabled = true;
        possibleItem.transform.position = originalHeight;
        possibleItem.transform.GetChild(2).transform.gameObject.SetActive(true);


        InteractiveItem item = possibleItem.gameObject.GetComponent<InteractiveItem>();
        item.setGrabed(false);
    }

    public void setInteractiveItemColliders(bool valueP, GameObject interactiveItem)
    {
        InteractiveItem item = interactiveItem.GetComponent<InteractiveItem>();
        item.setItem(valueP);
    }

    [PunRPC]
    public void possibleItemSolved()
    {
        if(possibleDropOffPlace != null && possibleItem != null)
        { 
            possibleItem.transform.parent = possibleDropOffPlace.transform;
            Vector3 newVectorZero = Vector3.zero;
            newVectorZero.y = 1.6f;
            possibleItem.transform.localPosition = newVectorZero;
            possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);
                //setInteractiveItemAnimation(true, possibleItem);
        }
        possibleItem = null;
        possibleDropOffPlace = null;
    }


    [PunRPC]
    protected bool tryDestroyCover ()
    {
        bool response = false;
        if (possibleItem != null)
        {
            Transform coverObjectTransform = possibleItem.transform.Find("KeyCover");
            if (coverObjectTransform != null)
            {
                GameObject coverObject = coverObjectTransform.gameObject;
                KeyCover keyCover = coverObject.GetComponent<KeyCover>();
                if (keyCover.keyCover != KeyCover.KEY_COVER_MATERIAL.NONE)
                    if (util != null)
                    {
                        Util nUtil = util.GetComponent<Util>();
                        keyCover.tryDestroy(nUtil);
                        response = keyCover.destroyed;
                        InteractiveItem item = possibleItem.GetComponent<InteractiveItem>();
                        if (item.currentColor != currentPlayerColor.currentColor)
                            possibleItem = null;
                    }
                    else
                        response = true;
            }
            else
                response = true;
        }
        return response;
    }


    void OnTriggerExit(Collider objectColliding)
    {
        //Debug.Log("Exiting with: " + objectColliding.gameObject.tag);
        if (objectColliding.gameObject.tag == "InteractuableObject")
        {
            //cameraUIHandler.setCanInteract(false);
            if(objectColliding.gameObject == possibleItem)
                possibleItem = null;
        }
        else if (objectColliding.gameObject.tag == "DropoffArea")
        {
            possibleDropOffPlace = null;
        }
    }

 

    public abstract void takeItem(bool newValue);

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }

    public void tutorialPress(bool pressed, bool isImage, int indexelement)
    {
        tutorialManager.changeColor(pressed,  isImage,  indexelement);
    }

}
