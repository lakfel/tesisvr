﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MotionManager : Photon.PunBehaviour
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Script that manages all the animations of an avatar
    public AnimationManager animationManager;

    // Current Device of the player
    public CurrentPlatform currentPlatform;

    // Bool the defines if the player can move or not
    public bool busyToWalk;

    public DBHandler DBHandler;

    public bool isMoving;


    //---------------------------------------
    // Private Variables
    //---------------------------------------

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start ()
    {
		
	}

    // Abstract method that initializes the motion mechanic defined
    public abstract void initializeMotionMechanic();

    // Method that do things according to the camera position
    public abstract void moveCamera(Transform camera);

    // Method that executes the main motion action
    public abstract void MotionAction();

    // Set Marker Status to On/Off
    public abstract void setMarkerStatus(bool newValue);
    // Update is called once per frame
    void Update () {
		
	}
}
