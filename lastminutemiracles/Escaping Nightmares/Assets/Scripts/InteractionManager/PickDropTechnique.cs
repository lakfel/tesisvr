﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickDropTechnique : PlayerInteraction
{
    private bool hasChanged;
    // Use this for initialization
    /*void Start()
    {
        
    }*/

    public override void takeItem(bool grabberHeld)
    {
        if (hasChanged != grabberHeld)
        {
            hasChanged = grabberHeld;
            if (possibleItem != null )
            {
                if (hasTakenItem == false)
                {
                    InteractiveItem item = possibleItem.GetComponent<InteractiveItem>();
                    Util.UTIL_ACTION currentAction = util.GetComponent<Util>().utilAction;
                    if (item.currentColor == currentPlayerColor.currentColor && currentAction.Equals(Util.UTIL_ACTION.HAND))
                    {
                        if (grabberHeld == true && hasTakenItem == false)
                        {
                            hasTakenItem = true;
                            if (PhotonNetwork.inRoom)
                            {
                                if (PhotonNetwork.connectionState == ConnectionState.Connected && photonView != null)
                                {
                                    photonView.RPC("PUNTakeItem", PhotonTargets.All, hasTakenItem);
                                }
                            }
                            else
                            {
                                PUNTakeItem(hasTakenItem);
                            }
                        }
                    }
                }
                else
                {
                    Debug.Log("Bota el item");
                    if (grabberHeld == false)
                    {
                        hasTakenItem = false;
                        if (PhotonNetwork.connectionState == ConnectionState.Connected && photonView != null)
                        {
                            if (photonView.isMine)
                            {
                                photonView.RPC("PUNTakeItem", PhotonTargets.All, hasTakenItem);
                            }
                        }
                        else
                        {
                            PUNTakeItem(hasTakenItem);
                        }
                    }
                }
            }
        }
    }


    [PunRPC]
    public void PUNTakeItem(bool toTakeItem)
    {
        // Debug.Log("toTakeItem: " + toTakeItem);

        if (toTakeItem == true)
        {
            setInteractiveItemColliders(false, possibleItem);
            //setInteractiveItemAnimation(false, possibleItem);
            possibleItem.transform.parent = grabPosition;
            possibleItem.transform.localPosition = Vector3.zero;
            possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);

        }
        else
        {
            if (possibleDropOffPlace != null)
            {
                PuzzleTwoObjects puzzleScript = possibleDropOffPlace.GetComponentInParent<PuzzleTwoObjects>();
                bool answer = puzzleScript.checkPuzzle(possibleItem);

                if (answer == true)
                {
                    PhotonView dropOffPhotonView = possibleDropOffPlace.GetComponent<PhotonView>();
                    if (PhotonNetwork.connectionState == ConnectionState.Connected && dropOffPhotonView != null)
                    {
                        dropOffPhotonView.RPC("solvePuzzle", PhotonTargets.All);
                        photonView.RPC("possibleItemSolved", PhotonTargets.All);
                    }
                    else
                    {
                        puzzleScript.solvePuzzle();
                        possibleItemSolved();
                    }
                }
                else
                {
                    // Notify that is wrong
                    setInteractiveItemColliders(true, possibleItem);
                    //setInteractiveItemAnimation(true, possibleItem);
                    possibleItem.transform.parent = GameObject.Find("InteractiveItems").transform;
                    possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);

                    Vector3 originalHeight = possibleItem.transform.position;
                    originalHeight.y = transform.position.y;
                    possibleItem.transform.position = originalHeight;
                }

            }
            else
            {
                setInteractiveItemColliders(true, possibleItem);
               // setInteractiveItemAnimation(true, possibleItem);
                possibleItem.transform.parent = GameObject.Find("InteractiveItems").transform;
                possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);

                Vector3 originalHeight = possibleItem.transform.position;
                originalHeight.y = transform.position.y;
                possibleItem.transform.position = originalHeight;
            }
            possibleItem = null;
        }
    }
/*
    public void setInteractiveItemColliders(bool valueP, GameObject interactiveItem)
    {
        InteractiveItem item = interactiveItem.GetComponent<InteractiveItem>();
        item.setItem(valueP);
    }

    public void setInteractiveItemAnimation(bool valueP, GameObject interactiveItem)
    {
        InteractiveItem item = interactiveItem.GetComponent<InteractiveItem>();
        item.setAnimation(valueP);
    }
    */
   /* [PunRPC]
    public void possibleItemSolved()
    {
        possibleItem.transform.parent = possibleDropOffPlace.transform;
        Vector3 newVectorZero = Vector3.zero;
        newVectorZero.y = 1.6f;
        possibleItem.transform.localPosition = newVectorZero;
        possibleItem.transform.localRotation = Quaternion.Euler(Vector3.zero);
        //setInteractiveItemAnimation(true, possibleItem);
        possibleItem = null;
        possibleDropOffPlace = null;
    }*/
}
