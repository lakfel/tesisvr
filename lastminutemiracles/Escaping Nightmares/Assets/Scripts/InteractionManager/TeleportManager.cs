﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class  TeleportManager : MotionManager, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // GameObject of the Teleporter Marker
    public GameObject teleportMarker;

    // Point from which the raycast will originate
    public Transform rayCastOrigin;

    // Length of the Ray Cast
    public float RayLength;

    // Rate at which the player will be walking when teleporting
    public float walkingSpeed;

    // Current mode for the avatar to teleport or to move. 0 == AVATAR MOVES. 1 == AVATAR TELEPORTS
    public int currentMode;

    // Particle System for teleporting. [0] For Player. [1] For Marker
    public GameObject[] teleportSpirals;


    
    //---------------------------------------
    // Private Variables
    //---------------------------------------

    // Main Camera of the Player
    private GameObject mainCameraTransform;

    // Bool that defines if the marker for teleporting is active or not
    private bool markerActive;

    private bool canMove;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start()
    {
        UnityEngine.Debug.Log("TeleportManager is ALIVE");
        switchNavMode();
        DBHandler.device = currentPlatform.platform.ToString();
        DBHandler.playerID = PhotonNetwork.player.NickName;
        isMoving = false;
    }
    private void Update()
    {
        DBHandler.playerID = PhotonNetwork.player.NickName;
    }
    public void switchNavMode()
    {
        currentMode = PlayerPrefs.GetInt("NavMode");
        if (currentMode == 1) 
        {
            currentMode = 0; // Mode A
        }
        else if (currentMode == 0)
        {
            currentMode = 1;  // Mode B
        }
        currentMode = 0;
        PlayerPrefs.SetInt("NavMode", currentMode);
    }

    public override void initializeMotionMechanic()
    {

    }

    /// <summary>
    /// Method that updates the camera position
    /// </summary>
    /// <param name="camera">Camera of the player</param>
    public override void moveCamera(Transform camera)
    {
        mainCameraTransform = camera.gameObject;
        Ray ray = new Ray();
        Ray rayLeft = new Ray();
        Ray rayRight = new Ray();
        Vector3 posLeft;
        Vector3 posRight; ;
        if (currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.OCULUS_TOUCH || currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.HTC_VIVE)
        {


            posLeft = rayCastOrigin.position;
            posRight = rayCastOrigin.position;
            posLeft.x -= 0.5f;
            posRight.x += 0.5f;
            ray = new Ray(rayCastOrigin.position, rayCastOrigin.forward);
            rayLeft = new Ray(posLeft, rayCastOrigin.forward);
            rayRight = new Ray(posRight, rayCastOrigin.forward);
            UnityEngine.Debug.DrawRay(rayCastOrigin.position, rayCastOrigin.forward, Color.yellow);

        }
        else if (currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.GEAR_VR)
        {


            posLeft = camera.position;
            posRight = camera.position;
            posLeft.x -= 0.5f;
            posRight.x += 0.5f;

            ray = new Ray(camera.position, camera.forward);

            rayLeft = new Ray(posLeft, camera.forward);
            rayRight = new Ray(posRight, camera.forward);
            UnityEngine.Debug.DrawRay(camera.position, camera.forward, Color.yellow);
        }
        RaycastHit hit, hitL, hitR;
        if (Physics.Raycast(ray, out hit, RayLength))
        {
            //Physics.Raycast(rayLeft, out hitL, RayLength);
            //Physics.Raycast(rayRight, out hitR, RayLength);

            //Debug.Log("Ray Hitted with: " + hit.collider.tag);
            Vector3 rayPoint = hit.point;
            rayPoint.y += 0.1f;
            UnityEngine.Debug.DrawLine(ray.origin, hit.point);
            //Debug.DrawLine(rayLeft.origin, hitL.point);
            //Debug.DrawLine(rayRight.origin, hitR.point);
            if (hit.collider.tag == "Floor" )//&& hitL.collider.tag == "Floor" && hitR.collider.tag == "Floor")
            {
               // Debug.Log("Acá SIIIIIIIIIII");
                //busyToWalk = false;
                canMove = true;
                if (!teleportMarker.activeSelf)
                {
                    teleportMarker.SetActive(true);
                }
                teleportMarker.transform.position = rayPoint;
            }
            else
            {
               // Debug.Log("Acá no se debería");
                //busyToWalk = true;
                teleportMarker.SetActive(false);
                canMove = false;
            }
        }
    }

    /// <summary>
    /// Method that enables/disables the marker
    /// </summary>
    /// <param name="newValue">New Status for the Marker</param>
    public override void setMarkerStatus(bool newValue)
    {
        bool theValue = newValue;
        if (busyToWalk == true)
        {
            theValue = false;
        }
        markerActive = theValue;
        teleportMarker.transform.GetChild(0).gameObject.SetActive(theValue);

        if(currentMode == 0 && theValue == true)
        {
           // teleportSpirals[1].SetActive(false);
        }
        else if(currentMode == 1 && theValue == true)
        {
            //teleportSpirals[1].SetActive(true);
        }
        else
        {
            //teleportSpirals[1].SetActive(false);
        }
        teleportMarker.SetActive(newValue);
    }

    /// <summary>
    /// Method that executes the motion action for this type of interaction
    /// </summary>
    public override void MotionAction()
    {
        PUNTeleportPlayer();
        //photonView.RPC("PUNTeleportPlayer", PhotonTargets.All);
    }

    /// <summary>
    /// Method that teleports the player where the marker is
    /// </summary>
    [PunRPC]
    public void PUNTeleportPlayer()
    {
        if (markerActive == true && busyToWalk == false && canMove == true)
        {
            Vector3 newPosition = teleportMarker.transform.position;
            if(currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.GEAR_VR || currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.OCULUS_TOUCH)
            {
                newPosition.y = mainCameraTransform.transform.parent.position.y;
            }
            else if(currentPlatform.platform == CurrentPlatform.CURRENT_PLATFORM.HTC_VIVE)
            {
                newPosition.y = mainCameraTransform.transform.parent.parent.position.y;
            }

            switch(currentMode)
            {
                case 0: // AVATAR WALKS
                    StartCoroutine(walkingCoroutine(mainCameraTransform.transform.parent.parent.gameObject, newPosition));
                    break;
                case 1: // AVATAR TELEPORTS
                    StartCoroutine(teleportingRoutine(mainCameraTransform.transform.parent.parent.gameObject, newPosition));
                    break;
            }
            //mainCameraTransform.transform.parent.position = newPosition;
        }
    }

    // Coroutine used to simulate the walking animation of the player
    IEnumerator walkingCoroutine(GameObject origin, Vector3 destination)
    {
        Stopwatch timeDelay = new Stopwatch();
        timeDelay.Start();
        isMoving = true;
        bool hasArrived = false;
        animationManager.setBoolAnim(true, 2, "Walk", true);
        busyToWalk = true;
        setMarkerStatus(false);
        while (hasArrived == false)
        {
            origin.transform.position = Vector3.MoveTowards(origin.transform.position, destination, Time.deltaTime * walkingSpeed);
            if (origin.transform.position == destination)
            {
                hasArrived = true;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
        busyToWalk = false;
        setMarkerStatus(true);
        animationManager.setBoolAnim(true, 2, "Walk", false);
        timeDelay.Stop();
        DBHandler.movementNumber++;
        DBHandler.totalMovementTime += timeDelay.ElapsedMilliseconds;
        DBHandler.sendDetail("movement", "yes", timeDelay.ElapsedMilliseconds);
        isMoving = false;
    }

    // Coroutine used to simulate the teleporting animation of the player
    IEnumerator teleportingRoutine(GameObject origin, Vector3 destination)
    {
        //Debug.Log("Is Teleporting Loco D: ");
        //teleportSpirals[0].SetActive(true);
        busyToWalk = true;
        setMarkerStatus(false);
        float distance = Vector3.Distance(destination, origin.transform.position);
        float possibleTime = distance / walkingSpeed;
        //Debug.Log("Posible tiempo es: " + possibleTime);

        yield return new WaitForSeconds(possibleTime);
        origin.transform.position = destination;

        //teleportSpirals[0].SetActive(false);
        busyToWalk = false;
        setMarkerStatus(true);
        //DBHandler.BModeJumps++;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "TutorialUmbral")
        {
            //switchNavMode();
            DBHandler.levelCompleted(0);
        }
        else if(other.tag == "SwitchMode")
        {
            //switchNavMode();
            DBHandler.levelCompleted(1);
        }
        else if(other.tag == "WinUmbral")
        {
            DBHandler.levelCompleted(2);
           // long totalTimeX = other.gameObject.GetComponent<WinnersAnnouncement>().totalTime;
            //DBHandler.sessionLength = totalTimeX + "";
            DBHandler.send();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
