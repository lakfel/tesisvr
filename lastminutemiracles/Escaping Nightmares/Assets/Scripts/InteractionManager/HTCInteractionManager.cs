﻿using UnityEngine;
public class HTCInteractionManager : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    

    // Script that manages all the htc Vive inputs
    public HTCInputManager htcInputManager;

    //---------------------------------------
    // Switchable Interactions Variables
    //---------------------------------------

    // Script that handles the teleportation process for the right arm
    public MotionManager motionManager;

    // Script that handles the interaction process for the left arm
    public PlayerInteraction playerInteractionManager;

    //Script that handles picking and storage actionable objects
    public UtilsPack utilPackMannager;

    //---------------------------------------
    // Private Variables
    //---------------------------------------

    public bool isAiming;

    public bool isOnPad;

    // Use this for initialization
    void Start()
    {
        
        if (GameObject.Find("IsTesting") != null)
        {
            htcInputManager = GameObject.Find("HTCCamera").GetComponent<HTCInputManager>();
        }
        else
        {
            string[] nameSplit = transform.parent.gameObject.name.Split(' ');
            string number = nameSplit[1];
            GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject temp in array)
            {
                if (temp.name.Contains("Camera") && temp.name.Contains(number))
                {
                    htcInputManager = temp.GetComponent<HTCInputManager>();
                    break;
                }
            }
        }

        if (htcInputManager != null)
        {
            htcInputManager.OnCameraMoveEvent += OnCameraMoved;
            htcInputManager.OnLeftTriggerEvent += OnLeftGrabber;
            htcInputManager.OnRightGrabberEvent += OnRightGrabber;
            htcInputManager.OnRightTriggerEvent += OnRightTriggerPressed;
            htcInputManager.OnRightTriggerUpEvent += OnRightTriggerUpPressed;
            htcInputManager.OnRightTouchpadPressedEvent += OnRightTouchpadPressed;
            htcInputManager.OnLeftTouchpadPressedEvent += OnLeftTouchpadPressed;
            htcInputManager.OnRightTouchpadUnPressedEvent += OnRightTouchpadUnPressed;
        }
    }

    /// <summary>
    /// Method used to update the camera position 
    /// </summary>
    /// <param name="playerCamera">The Camera Position</param>
    public void OnCameraMoved(Transform playerCamera)
    {
        motionManager.moveCamera(playerCamera);
    }

    public void OnRightTouchpadPressed()
    {
        if (!motionManager.isMoving)
        {
            playerInteractionManager.tutorialPress(true, false, 3);
            playerInteractionManager.nextUtil();
            motionManager.DBHandler.sendDetail("menu mode", "yes", 0);
            refreshLeftHand();
        }
        else
        {
            motionManager.DBHandler.sendDetail("menu mode", "not, walking", 0);
        }

    }

    public void OnRightTouchpadUnPressed()
    {
        if (!motionManager.isMoving)
            playerInteractionManager.tutorialPress(false, false, 3);
    }

    public void OnLeftTouchpadPressed()
    {
        //circularMenuMannager.moveMenu();
        //Action Available
        //motionManager.animationManager.setActivAnim(true, 3, true);
        //motionManager.animationManager.setBoolAnim(true, 3, "IsGreeting", true);
    }

    public void OnRightGrabber(bool isGrabbing)
    {
        
        playerInteractionManager.tutorialPress(isGrabbing, false, 1);
        motionManager.animationManager.setBoolAnim(true, 1, "ClosedHand", isGrabbing);
        if(motionManager.busyToWalk == false)
        {
            if (isOnPad == true)
            {
                isAiming = isGrabbing;
                motionManager.setMarkerStatus(isGrabbing);
            }
            else if (isOnPad == false)
            {
                isAiming = false;
                motionManager.setMarkerStatus(false);
            }
            motionManager.DBHandler.sendDetail("movement mode", "yes", 0);
        }
        else
        {
            isAiming = isGrabbing;
            motionManager.setMarkerStatus(isGrabbing);
            motionManager.DBHandler.sendDetail("movement mode", "not, busy to walk", 0);

        }
    }

    public void OnLeftGrabber(bool isGrabbing)
    {
            playerInteractionManager.tutorialPress(isGrabbing, false, 0);
        if (isGrabbing)
        {
            if (!motionManager.isMoving)
            {
                motionManager.DBHandler.sendDetail("interation", "yes", 0);
                //playerInteractionManager.takeItem(isGrabbing);
                playerInteractionManager.interaction();
                refreshLeftHand();
            }
            else
            {
                motionManager.DBHandler.sendDetail("interation", "not, walking", 0);
            }

        }
    }

    public void OnRightTriggerPressed()
    {

        playerInteractionManager.tutorialPress(true, false, 2);
        if (isAiming == true)
        {
            motionManager.MotionAction();
        }
        else
        {
            motionManager.DBHandler.sendDetail("movement mode", "not, not aiming", 0);
        }
    }
    public void OnRightTriggerUpPressed()
    {
        playerInteractionManager.tutorialPress(false, false, 2);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "HTCPlatform")
        {
            isOnPad = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "HTCPlatform")
        {
            isOnPad = false;
            motionManager.setMarkerStatus(false);
        }
    }

    public void refreshLeftHand()
    {
        if (!playerInteractionManager.currentStatus.Equals(PlayerInteraction.LEFT_HAND_STATUS.NONE))
            motionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", true);
        else
            motionManager.animationManager.setBoolAnim(true, 0, "ClosedHand", false);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }

}
