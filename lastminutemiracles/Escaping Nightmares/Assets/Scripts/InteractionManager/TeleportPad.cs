﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPad : MonoBehaviour
{
    public TeleportManager teleportManager;

    public bool canAimToTeleport;

    private void OnTriggerEnter(Collider other)
    {
        canAimToTeleport = true;
    }

    private void OnTriggerExit(Collider other)
    {
        canAimToTeleport = true;
        teleportManager.setMarkerStatus(false);
    }
}
