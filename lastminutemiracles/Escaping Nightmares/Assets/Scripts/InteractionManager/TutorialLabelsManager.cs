﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class TutorialLabelsManager : Photon.PunBehaviour
{
    public GameObject[] labels;
    public Color originalcolor;
    public LvlManager.Platform platform;

    // Order Vive
    /* RC = Right Control, LC = Left Control
     *  1) LC press trigger (Actions)
     *  2) RC grip
     *  3) RC Trigger
     *  4) RC touchpad
     * */

    // Order GEAR
    /* S = Swap
     *  1) S right. Nav mode text
     *  2) S right. Nav mode image
     *  3) S left. Interaction mode text
     *  4) S left. Interaction mode image
     *  5) S up. greet text
     *  6) S up. greet image
     *  7) S down. change util text
     *  8) S down. change util image
     *  9) Tap. action text
     *  10) Tap. action util image
     * */

    public void changeColor(bool pressed, bool isImage, int indexelement)
    {
        Color newcolor = originalcolor;
        Color newcolorImage = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        if (pressed)
        {
            newcolor = new Color(1.0f, 0.435f, 0.435f, 1.0f);
            newcolorImage = new Color(1.0f, 0.435f, 0.435f, 1.0f);
        }
        

        if(isImage)
        {
            Image nImage = labels[indexelement].GetComponent<Image>();
            nImage.color = newcolorImage;
        }
        else
        {
            Text nText = labels[indexelement].GetComponent<Text>();
            nText.color = newcolor;
        }
    }

    

    private void Start()
    {
        Debug.Log("Tutorial Label CREATED");
    }

}

