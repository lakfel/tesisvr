﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class CircularMenuMannager : Photon.PunBehaviour, IPunObservable
{

    public UtilsPack utilsPack;
    public PlayerInteraction playerInteraction;

    public Material normalStatusMaterial;
    public Material selectedStatusMaterial;
    public Material pressedStatusMaterial;

    public Material utilNormalStatusMaterial;
    public Material utilSelectedStatusMaterial;
    public Material utilPressedStatusMaterial;

    // The menu to be mannaged    
    public GameObject menu;
    // The actual status of visibility fo the menu
    public bool shown;
    private int menuItems;
    private int currentItem;

    // It indicates the number of submenus openned
    private int deeplevel;

    private List<GameObject> currentMenu;

    private void Start()
    {
        if (menu != null)
        {
            menu.SetActive(false);
            shown = false;
            menuItems = menu.transform.childCount;
            deeplevel = 0;
            currentMenu = new List<GameObject>();
            currentMenu.Clear();
            currentMenu.Add(menu.transform.GetChild(0).gameObject);
            currentMenu.Add(menu.transform.GetChild(1).gameObject);
            currentMenu.Add(menu.transform.GetChild(2).gameObject);
        }
    }




    private void Update()
    {

    }


    private void Awake()
    {

    }

    public void refreshMenu()
    {
        int children = currentMenu.Count;
        for (int i = 0; i < children; ++i)
        {
            Transform child = menu.transform.GetChild(i);
            GameObject menuItem = currentMenu[i];
            GameObject sphere = menuItem.transform.GetChild(0).gameObject;
            Renderer renderer = sphere.GetComponent<Renderer>();
            if (i == currentItem)
            {
                if(deeplevel == 1)
                    renderer.material = selectedStatusMaterial;
                else
                    renderer.material = utilSelectedStatusMaterial;
            }
            else
            {
                if (deeplevel == 2)
                    renderer.material = normalStatusMaterial;
                else
                    renderer.material = utilNormalStatusMaterial;
            }
        }
    }

    public void ExitMenu()
    {
        deeplevel = 0;
        currentItem = -1;
        shown = false;
        menu.SetActive(shown);
        refreshMenu();
    }

    public CircularMenuItem.MENU_ITEM_ACTION updateMenu()
    {
        CircularMenuItem.MENU_ITEM_ACTION response = CircularMenuItem.MENU_ITEM_ACTION.UTIL_NONE;
        if (menu != null )
        {
            if (shown && currentItem > -1)
            {
                CircularMenuItem current = currentMenu[currentItem].GetComponent<CircularMenuItem>();
                CircularMenuItem.MENU_ITEM_ACTION action = current.currentAction;
                response = action;
                Renderer renderer = current.transform.GetChild(0).gameObject.GetComponent<Renderer>();
                if (deeplevel == 1)
                    renderer.material = pressedStatusMaterial;
                else
                {
                    renderer.material = utilPressedStatusMaterial;
                }

                if (action.Equals(CircularMenuItem.MENU_ITEM_ACTION.EXIT))
                {
                    ExitMenu();
                }
                else if (action.Equals(CircularMenuItem.MENU_ITEM_ACTION.EXIT_PACK))
                {
                    deeplevel = 1;
                    currentItem = -1;

                    menu.transform.GetChild(0).gameObject.SetActive(true);
                    menu.transform.GetChild(1).gameObject.SetActive(true);
                    menu.transform.GetChild(2).gameObject.SetActive(true);
                    menu.transform.GetChild(3).gameObject.SetActive(false);
                    menu.transform.GetChild(4).gameObject.SetActive(false);
                    menu.transform.GetChild(5).gameObject.SetActive(false);
                    menu.transform.GetChild(6).gameObject.SetActive(false);
                    
                    currentMenu.Clear();
                    currentMenu.Add(menu.transform.GetChild(0).gameObject);
                    currentMenu.Add(menu.transform.GetChild(1).gameObject);
                    currentMenu.Add(menu.transform.GetChild(2).gameObject);

                    refreshMenu();
                }
                else if (action.Equals(CircularMenuItem.MENU_ITEM_ACTION.GREET))
                {
                    response = CircularMenuItem.MENU_ITEM_ACTION.GREET;
                    ExitMenu();
                }
                else if (action.Equals(CircularMenuItem.MENU_ITEM_ACTION.OPEN_UTILS_PACK))
                {
                    deeplevel = 2;
                    currentItem = -1;

                    menu.transform.GetChild(0).gameObject.SetActive(false);
                    menu.transform.GetChild(1).gameObject.SetActive(false);
                    menu.transform.GetChild(2).gameObject.SetActive(false);
                    menu.transform.GetChild(3).gameObject.SetActive(true);
                    menu.transform.GetChild(4).gameObject.SetActive(true);
                    menu.transform.GetChild(5).gameObject.SetActive(true);
                    menu.transform.GetChild(6).gameObject.SetActive(true);

                    currentMenu.Clear();
                    currentMenu.Add(menu.transform.GetChild(3).gameObject);
                    currentMenu.Add(menu.transform.GetChild(4).gameObject);
                    currentMenu.Add(menu.transform.GetChild(5).gameObject);
                    currentMenu.Add(menu.transform.GetChild(6).gameObject);

                    refreshMenu();
                }
                else
                {
                    GameObject utilObject = null;
                    if (action.Equals(CircularMenuItem.MENU_ITEM_ACTION.UTIL_HAMMER))
                        utilObject = utilsPack.getUtil(Util.UTIL_ACTION.ROCK_BREAKER);
                    if (action.Equals(CircularMenuItem.MENU_ITEM_ACTION.UTIL_TORCH))
                        utilObject = utilsPack.getUtil(Util.UTIL_ACTION.ICE_MELTER);

                    playerInteraction.selectUtilObject(utilObject);
                    ExitMenu();
                }
            }
            else
            {
                deeplevel = 1;
                currentItem = -1;
                shown = true;
                currentMenu.Clear();
                currentMenu.Add(menu.transform.GetChild(0).gameObject);
                currentMenu.Add(menu.transform.GetChild(1).gameObject);
                currentMenu.Add(menu.transform.GetChild(2).gameObject);
                menu.SetActive(shown);
                refreshMenu();
            }

        }
        return response;

    }

    public void moveMenu()
    {
        currentItem++;
        currentItem = currentItem % currentMenu.Count;
        refreshMenu();

    }



    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }

}


