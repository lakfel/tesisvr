﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AITesting : MonoBehaviour
{
    public Transform destino;

    public float speedo;

    public bool hasArrived;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(moveToTarget());
	}

    IEnumerator moveToTarget()
    {
        while(hasArrived == false)
        {
            Debug.Log("Cuando me muevo?");
            Vector3 destinyPosition = destino.position;
            destinyPosition.y = transform.position.y;
            transform.position = Vector3.MoveTowards(transform.position, destinyPosition, Time.deltaTime * speedo);
            if(transform.position == destinyPosition)
            {
                hasArrived = true;
            }
            yield return new WaitForSeconds(Time.deltaTime);
        }
    }

	
	// Update is called once per frame
	void Update ()
    {
        
	}
}
