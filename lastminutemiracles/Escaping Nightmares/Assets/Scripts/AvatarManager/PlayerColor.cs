﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColor : Photon.PunBehaviour, IPunObservable
{
    public SkinnedMeshRenderer playerMeshRenderer;

    public ObjectColor.ITEM_COLOR currentColor;

    private Color32 playerMatcolor;

	// Use this for initialization
	void Start ()
    {
        playerMatcolor = new Color32(255, 255, 255, 255);
        currentColor = ObjectColor.ITEM_COLOR.WHITE;
        playerMeshRenderer.material.color = playerMatcolor;
	}

    [PunRPC]
    public void setNewcolor(Color32 newColor)
    {
        playerMatcolor = newColor;
        Material neoMaterial = playerMeshRenderer.material;
        neoMaterial.color = playerMatcolor;
        playerMeshRenderer.material.color = newColor;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.name == "Platform")
        {
            Debug.Log("Alerta COLOR CAMBIANDO");
            ObjectColor neoColor = other.gameObject.GetComponent<ObjectColor>();
            setNewcolor(neoColor.getColor());
            currentColor = neoColor.currentColor;
            other.transform.parent.transform.gameObject.SetActive(false);
           // photonView.RPC("setNewColor", PhotonTargets.All, neoColor.getColor());
        }
    }

    // Update is called once per frame
    void Update () {
		
	}

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
