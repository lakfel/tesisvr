﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class UtilsPack : Photon.PunBehaviour
{
  
    public List<GameObject> utils;

    public int currentUtil;

    public PlayerInteraction playerInteraction;

    public GameObject utilTips;

    private void Start()
    {
        //utils = new List<GameObject>();
        currentUtil = 0;
    }

    public void pickUtil(GameObject newUtil)
    {
        utils.Add(newUtil);
        // Hide the interactuable target object
        newUtil.GetComponent<SphereCollider>().enabled = false;
        newUtil.transform.GetChild(1).transform.gameObject.SetActive(false);
        newUtil.SetActive(false);
        Debug.Log("PICKING UTIL :" + newUtil.name);
    }

    public GameObject nextUtil()
    {
        GameObject response = null;

        GameObject[] ops = { utilTips.transform.GetChild(0).gameObject, utilTips.transform.GetChild(1).gameObject, utilTips.transform.GetChild(2).gameObject };
        GameObject[] txts = { ops[0].transform.GetChild(0).transform.GetChild(0).gameObject, ops[1].transform.GetChild(0).transform.GetChild(0).gameObject, ops[2].transform.GetChild(0).transform.GetChild(0).gameObject};

        currentUtil = (currentUtil + 1) % utils.Count;
        ops[0].SetActive(true);
        ops[1].SetActive(false);
        ops[2].SetActive(false);
        if (utils.Count == 1)
        {
            txts[0].GetComponent<TextMesh>().text = utils[currentUtil].GetComponent<Util>().textUtil;   
        }
        else
        {
            Debug.Log("Current UTIL :" + currentUtil);
            Debug.Log("UTILcount :" + utils.Count);
            if (currentUtil == 1)
            {
                txts[0].GetComponent<TextMesh>().text = utils[1].GetComponent<Util>().textUtil;
                ops[2].SetActive(true);
                txts[2].GetComponent<TextMesh>().text = utils[0].GetComponent<Util>().textUtil;
            }
            else
            {
                txts[0].GetComponent<TextMesh>().text = utils[0].GetComponent<Util>().textUtil;
                ops[1].SetActive(true);
                txts[1].GetComponent<TextMesh>().text = utils[01].GetComponent<Util>().textUtil;
            }

        }
        utilTips.SetActive(true);
        utilTips.GetComponent<Animator>().SetBool("showMenu", true);
        
        response = utils[currentUtil];
        return response;
    }

    public GameObject getUtil (Util.UTIL_ACTION action)
    {
        GameObject searched = null;
        for(int i = 0; i < utils.Count && searched ==  null; i++)
        {
            GameObject current = utils[i];
            Util utilObject = current.GetComponent<Util>();
            if(utilObject != null && utilObject.utilAction.Equals(action))
            {
                searched = current;
            }
        }
        return searched;
        
    }




}
