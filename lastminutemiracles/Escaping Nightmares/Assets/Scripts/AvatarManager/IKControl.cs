﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class IKControl : Photon.PunBehaviour, IPunObservable
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // Main Animator of the player
    protected Animator animator;

    // Bool that defines if the ik is going to be active
    public bool ikActive = false;

    public bool ikActiveOthers;

    // Transform for the right ik hand
    public Transform rightHandObj = null;

    // Transform for the left ik hand
    public Transform leftHandObj = null;

    // Transform for the ik head
    public Transform lookHeadObj = null;


    public int contDEBUG;
    //---------------------------------------
    // Methods
    //---------------------------------------
    void Start()
    {
        contDEBUG = 0;
        animator = GetComponent<Animator>();
        if (GameObject.Find("IsTesting") == null)
        {
            setParts();
        }
    }

    /// <summary>
    /// Method that sets the transform of the current parts 
    /// </summary>
    public void setParts()
    {
        GameObject[] playerParts = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject temp in playerParts)
        {
            string[] nameSplit = name.Split(' ');
            string number = nameSplit[1];
            if (temp.name.Contains("Camera") && temp.name.Contains(number))
            {
                AnchorsManagement anch = temp.GetComponent<AnchorsManagement>();
                lookHeadObj = anch.anchors[0];
                leftHandObj = anch.anchors[1];
                rightHandObj = anch.anchors[2];
            }
        }
    }

    [PunRPC]
    public void envioMSN()
    {
        string txt = "";
        contDEBUG++;
        if (!GetComponent<PhotonView>().isMine)
            txt = contDEBUG + ". IKCONTROLS Camara REMOTO : ";
        else
            txt = contDEBUG + ". IKCONTROLS Camara LOCAL: ";
        txt += "\n\t" + lookHeadObj.transform.position.y + " - " + lookHeadObj.transform.rotation.x
                + " - " + lookHeadObj.transform.rotation.y + " - " + lookHeadObj.transform.rotation.z
                + " - " + lookHeadObj.transform.rotation.w;
        if (!GetComponent<PhotonView>().isMine)
            GetComponent<PhotonView>().RPC("depuracionRemota", PhotonTargets.Others, new object[] { txt });
        else
            Debug.Log(txt);


    }

    [PunRPC]
    public void depuracionRemota(string mensaje)
    {
        Debug.Log(mensaje);
    }

    //a callback for calculating IK
    void OnAnimatorIK()
    {
        if (animator)
        {
            //if the IK is active, set the position and rotation directly to the goal. 
            if (ikActive)
            {

                // Set the look target position, if one has been assigned
                if (lookHeadObj != null)
                {
                    Vector3 pos2 = new Vector3(lookHeadObj.position.x, 56, lookHeadObj.position.z);
                    animator.SetLookAtWeight(1);
                    animator.SetLookAtPosition(lookHeadObj.position);

                    if(GetComponent<CurrentPlatform>().platform.Equals(CurrentPlatform.CURRENT_PLATFORM.HTC_VIVE) && false)
                    {
                        
                            GetComponent<PhotonView>().RPC("envioMSN", PhotonTargets.All);
                       

                    }
                    

                }

                // Set the right hand target position and rotation, if one has been assigned
                if (rightHandObj != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    //rightHandObj.position = new Vector3(rightHandObj.position.x, 0, rightHandObj.position.z);
                    //rightHandObj.rotation = new Quaternion(0, rightHandObj.rotation.y, 0,rightHandObj.rotation.w);
                    animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandObj.rotation);
                }
                if (leftHandObj != null)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandObj.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandObj.rotation);
                }
            }

            //if the IK is not active, set the position and rotation of the hand and head back to the original position
            else
            {
                animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
                animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
                animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
                animator.SetLookAtWeight(0);
            }
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
