﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class Util: Photon.PunBehaviour
{
    // List of actions of different utils
    // TODO. This should be stored in a config file to load and make th SW configurable
    public enum UTIL_ACTION {HAND,ICE_MELTER, ROCK_BREAKER};

    //---------------------------------------
    // Public Variables
    //---------------------------------------

    // The SphereCollider that trigger the events
    public Rigidbody itemRigiBody;

    // Action of the util
    public UTIL_ACTION utilAction;

    public string textUtil;

    //---------------------------------------
    // Methods
    //---------------------------------------

    private void Start()
    {
    }

    /// <summary>
    /// Method that enables/disables the colliders of the item
    /// </summary>
    /// <param name="valueP">Value to be set to the colliders</param>
    public void setItem(bool valueP)
    {
        itemRigiBody.detectCollisions = valueP;
    }

    /// <summary>
    /// Method that modifies the item animation
    /// </summary>
    /// <param name="valueP"></param>
    public void setAnimation(bool valueP)
    {
        GetComponentInChildren<Animator>().SetBool("Moving", valueP);
    }




}

