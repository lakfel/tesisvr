﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentPlatform : MonoBehaviour
{
    public enum CURRENT_PLATFORM { PC, OCULUS_TOUCH, GEAR_VR, HTC_VIVE};

    public CURRENT_PLATFORM platform;
}
