﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OculusInputManager : Photon.PunBehaviour
{
    //---------------------------------------
    // Delegates and Events
    //---------------------------------------    

    public GameObject mainCamera;

    public delegate void OnCameraMove(Transform cameraTransform);
    public event OnCameraMove OnCameraMoveEvent;

    // Gear VR---------------------------------------
    public delegate void OnTouchCenter();
    public event OnTouchCenter OnTouchCenterEvent;

    public delegate void OnTouchLeft();
    public event OnTouchLeft OnTouchLeftEvent;

    public delegate void OnTouchRight();
    public event OnTouchRight OnTouchRightEvent;

    public delegate void OnTouchUp();
    public event OnTouchUp OnTouchUpEvent;

    public delegate void OnTouchDown();
    public event OnTouchDown OnTouchDownEvent;

    // Oculus Touch------------------------------------

    public delegate void OnRightGrabber(bool isGrabbing);
    public event OnRightGrabber OnRightGabbrerBegin;

    public delegate void OnLeftGrabber(bool isGrabbing);
    public event OnLeftGrabber OnLeftGrabberBegin;

    public delegate void OnRightTrigger();
    public event OnRightTrigger OnRightTriggerBegin;

    public delegate void onRightAPressed();
    public event onRightAPressed onRightAPressedEvent;

    public delegate void onLeftXPressed();
    public event onLeftXPressed onLeftXPressedEvent;

    private bool rightTriggerLock;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Update is called once per frame
    void FixedUpdate()
    {
        if (GameObject.Find("IsTesting") != null || (PhotonNetwork.connected == true && photonView != null && photonView.isMine))
        {
            if(OnCameraMoveEvent != null)
            {
                OnCameraMoveEvent(mainCamera.transform);
            }
            switch (GetComponent<CurrentPlatform>().platform)
            {
                case CurrentPlatform.CURRENT_PLATFORM.OCULUS_TOUCH: // OCULUS TOUCH
                    if(OnRightGabbrerBegin != null)
                    {
                        float rightGrabberValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.RTouch);
                        //Debug.Log("Grab Right Value: " + rightGrabberValue);
                        if (rightGrabberValue > 0.85f)
                        {
                            OnRightGabbrerBegin(true);
                        }
                        else
                        {
                            OnRightGabbrerBegin(false);
                        }
                    }

                    if(OnLeftGrabberBegin != null)
                    {
                        float leftGrabberValue = OVRInput.Get(OVRInput.Axis1D.PrimaryHandTrigger, OVRInput.Controller.LTouch);
                        if (leftGrabberValue > 0.85f)
                        {
                            OnLeftGrabberBegin(true);
                        }
                        else if (leftGrabberValue > 0f && leftGrabberValue < 0.1)
                        {
                            OnLeftGrabberBegin(false);
                        }
                    }


                    if (rightTriggerLock == false && OnRightTriggerBegin != null)
                    {
                        float rightShootTrigger = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
                        if (rightShootTrigger > 0.9f)
                        {
                            StartCoroutine(applyRightTriggerDelay(0.8f));
                            OnRightTriggerBegin();
                        }
                    }

                    break;
                case CurrentPlatform.CURRENT_PLATFORM.GEAR_VR: // GEAR VR
                    // If im the local player or if i'm in a menu receive oculus input
                    if (OVRInput.GetDown(OVRInput.Button.One) && OnTouchCenterEvent != null)
                    {
                        if(rightTriggerLock == false && OnTouchCenterEvent != null)
                        {
                            OnTouchCenterEvent();
                            StartCoroutine(applyRightTriggerDelay(0.8f));
                        }
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Left) && OnTouchLeftEvent != null)
                    {
                        OnTouchLeftEvent();
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Right) && OnTouchRightEvent != null)
                    {
                        OnTouchRightEvent();
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Up) && OnTouchUpEvent != null)
                    {
                        OnTouchUpEvent();
                    }
                    else if (OVRInput.GetDown(OVRInput.Button.Down) && OnTouchDownEvent != null)
                    {
                        OnTouchDownEvent();
                    }
                    break;
            }
        }
        
        if(GameObject.Find("IsTesting") != null)
        {
            Debug.Log("Is Testing D:");
            if(OnTouchDownEvent != null)
            {
                if(Input.GetKeyDown(KeyCode.Space))
                {
                    OnTouchDownEvent();
                }
            }
        }
    }

    IEnumerator applyRightTriggerDelay(float delay)
    {
        rightTriggerLock = true;
        yield return new WaitForSeconds(delay);
        rightTriggerLock = false;
    }
}
