﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HTCInputManager : Photon.PunBehaviour
{

    public GameObject HTCCamera;

    public GameObject rightHand;

    private SteamVR_TrackedObject RTracked;

    private SteamVR_Controller.Device RController
    {
        get { return SteamVR_Controller.Input((int)RTracked.index); }
    }

    public GameObject leftHand;

    private SteamVR_TrackedObject LTracked;

    private SteamVR_Controller.Device LController
    {
        get { return SteamVR_Controller.Input((int)LTracked.index); }
    }

    public delegate void OnCameraMove(Transform cameraPosition);
    public event OnCameraMove OnCameraMoveEvent;

    public delegate void OnRightGrabber(bool isGrabbing);
    public event OnRightGrabber OnRightGrabberEvent;

    public delegate void OnRightTrigger();
    public event OnRightTrigger OnRightTriggerEvent;

    public delegate void OnRightTriggerUp();
    public event OnRightTriggerUp OnRightTriggerUpEvent;

    public delegate void OnLeftTrigger(bool isGrabbing);
    public event OnLeftTrigger OnLeftTriggerEvent;

    public delegate void onRightTouchpadPressed();
    public event onRightTouchpadPressed OnRightTouchpadPressedEvent;
    
    public delegate void onLeftTouchpadPressed();
    public event onLeftTouchpadPressed OnLeftTouchpadPressedEvent;

    public delegate void onRightTouchpadUnPressed();
    public event onRightTouchpadUnPressed OnRightTouchpadUnPressedEvent;

    public int contDEBUG;

    // Use this for initialization
    void Awake ()
    {
        RTracked = rightHand.GetComponent<SteamVR_TrackedObject>();
        LTracked = leftHand.GetComponent<SteamVR_TrackedObject>();
	}

    private void Start()
    {
        contDEBUG = 0;
        Vector3 currentPost = transform.position;
        currentPost.y -= 1.8f;
        transform.position = currentPost;
    }


    [PunRPC]
    public void envioMSN()
    {
        string txt = "";
        contDEBUG++;
        if (!GetComponent<PhotonView>().isMine)
            txt = contDEBUG + ". HTCINPUT Camara REMOTO : ";
        else
            txt = contDEBUG + ". HTCINPUT Camara LOCAL: ";
        txt += "\n\t" + HTCCamera.transform.position.x + " - " + HTCCamera.transform.position.y + " - " + HTCCamera.transform.position.z + " - " + HTCCamera.transform.rotation.x
                + " - " + HTCCamera.transform.rotation.y + " - " + HTCCamera.transform.rotation.z
                + " - " + HTCCamera.transform.rotation.w;
        if (!GetComponent<PhotonView>().isMine && false)
            GetComponent<PhotonView>().RPC("depuracionRemota", PhotonTargets.Others, new object[] { txt });
        else if(false)
            Debug.Log(txt);


    }

    [PunRPC]
    public void depuracionRemota(string mensaje)
    {
        Debug.Log(mensaje);
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (GameObject.Find("IsTesting") != null || (PhotonNetwork.connected == true && photonView != null && photonView.isMine))
        {
            if (OnCameraMoveEvent != null)
            {
                //Debug.Log("Camera is: " + HTCCamera.transform.position.ToString());
                OnCameraMoveEvent(HTCCamera.transform);
                         
                 GetComponent<PhotonView>().RPC("envioMSN", PhotonTargets.All);
                
            }
            if (LController.GetHairTriggerDown() && OnLeftTriggerEvent != null)
            {
                //Debug.Log(gameObject.name + " Trigger Left Something");
                OnLeftTriggerEvent(true);
            }
            else if (LController.GetHairTriggerUp() && OnLeftTriggerEvent != null)
            {
                //Debug.Log(gameObject.name + " Trigger Left Something");
                OnLeftTriggerEvent(false);
            }

            if (RController.GetHairTriggerDown() && OnRightTriggerEvent != null)
            {
                // Debug.Log(gameObject.name + " Trigger Right Something");
                OnRightTriggerEvent();
            }
            else if (RController.GetHairTriggerUp() && OnRightTriggerUpEvent != null)
            {
                // Debug.Log(gameObject.name + " Trigger Right Something");
                OnRightTriggerUpEvent();
            }

            if (RController.GetPressDown(SteamVR_Controller.ButtonMask.Grip) && OnRightGrabberEvent != null)
            {
                OnRightGrabberEvent(true);
            }
            else if(RController.GetPressUp(SteamVR_Controller.ButtonMask.Grip) && OnRightGrabberEvent != null)
            {
                OnRightGrabberEvent(false);
            }

            if (RController.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad) && OnRightTouchpadPressedEvent != null)
            {
                OnRightTouchpadPressedEvent();
            }
            else if (RController.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && OnRightTouchpadUnPressedEvent != null)
            {
                OnRightTouchpadUnPressedEvent();
            }

            if (LController.GetPressUp(SteamVR_Controller.ButtonMask.Touchpad) && OnLeftTouchpadPressedEvent != null)
            {
                OnLeftTouchpadPressedEvent();
            }


        }            
    }
}
