﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class KeyCover : MonoBehaviour
{

    public enum KEY_COVER_MATERIAL {ICE, ROCK, NONE };

    public KEY_COVER_MATERIAL keyCover;

    public bool destroyed;

    public GameObject cover;

    private void Start()
    {
        if (keyCover == KEY_COVER_MATERIAL.NONE)
            destroyed = true;
        else
            destroyed = false;

        cover = gameObject.transform.GetChild(0).gameObject;
    }

    [PunRPC]
    public void tryDestroy(Util util)
    {
        Util.UTIL_ACTION utilAction = util.utilAction;
        destroyed = destroyed 
                    || keyCover.Equals(KEY_COVER_MATERIAL.NONE)
                    || (keyCover.Equals(KEY_COVER_MATERIAL.ICE) && utilAction.Equals(Util.UTIL_ACTION.ICE_MELTER))
                    || (keyCover.Equals(KEY_COVER_MATERIAL.ROCK) && utilAction.Equals(Util.UTIL_ACTION.ROCK_BREAKER));

        cover.SetActive(!destroyed);
    }

}

