﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveItem : Photon.PunBehaviour
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------

    public ObjectColor.ITEM_COLOR currentColor;

    // Sphere Collider of the Item
    public SphereCollider itemSphereCollider;


    // It says if the object is being grabed by a player
    private bool grabed;


    private void Start()
    {
        grabed = false;
    }

    //---------------------------------------
    // Methods
    //---------------------------------------

    public void setGrabed (bool pIsGrabed)
    {
        grabed = pIsGrabed;
    }

    public bool isGrabed()
    {
        return grabed;
    }

    /// <summary>
    /// Method that enables/disables the colliders of the item
    /// </summary>
    /// <param name="valueP">Value to be set to the colliders</param>
    public void setItem(bool valueP)
    {
        itemSphereCollider.enabled = valueP;
    }
    
    /// <summary>
    /// Method that modifies the item animation
    /// </summary>
    /// <param name="valueP"></param>
    public void setAnimation(bool valueP)
    {
        //GetComponentInChildren<Animator>().SetBool("Moving", valueP);
    }
}
