﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectColor : MonoBehaviour
{
    public enum ITEM_COLOR { WHITE, CYAN, RED, GREEN, YELLOW, MAGENTA};

    public ITEM_COLOR currentColor;

    public Color32 getColor()
    {
        Color32 answer = new Color32();
        switch(currentColor)
        {
            case ITEM_COLOR.CYAN:
                answer = Color.cyan;
                break;
            case ITEM_COLOR.RED:
                answer = Color.red;
                break;
            case ITEM_COLOR.GREEN:
                answer = Color.green;
                break;
            case ITEM_COLOR.YELLOW:
                answer = Color.yellow;
                break;
            case ITEM_COLOR.MAGENTA:
                answer = Color.magenta;
                break;
        }
        return answer;
    }
	


}
