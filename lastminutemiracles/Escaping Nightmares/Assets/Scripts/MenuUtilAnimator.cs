﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUtilAnimator : StateMachineBehaviour {

 

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //animator.gameObject.SetActive(true);
        if(stateInfo.IsName("Base.ShowMenu"))
            animator.SetBool("showMenu", false);
        
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.IsName("Base.ShowMenu"))
        {
            if (!animator.GetBool("showMenu"))
                animator.gameObject.SetActive(false);
        }
        else if (animator.transform.root.GetComponent<CurrentPlatform>().Equals(CurrentPlatform.CURRENT_PLATFORM.GEAR_VR))
        {
            GameObject interactionMannager = animator.transform.root.GetChild(1).gameObject;
            GearInteractionManager gearInteraction = interactionMannager.GetComponent<GearInteractionManager>();
            gearInteraction.currentMode = GearInteractionManager.CURRENT_GEAR_MODE.INTERACTION;
        }

        //animator.SetBool("showMenu", false);
    }

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
