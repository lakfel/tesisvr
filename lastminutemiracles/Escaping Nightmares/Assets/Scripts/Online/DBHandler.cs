﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Diagnostics;

public class DBHandler : Photon.PunBehaviour, IPunObservable
{
    private string URLGeneral = "https://docs.google.com/forms/d/e/1FAIpQLSfcpcrtM6BZ5qzowvXMDzFP_109qytLs6jTakS-k4MxgNrrZA/formResponse";
    private string URLDetalle = "https://docs.google.com/forms/d/e/1FAIpQLSc4DR9g9hXboHTvGFj7vTjt30jCQa-m6pKen04Ahe0xxh60nA/formResponse";
    // Use this for initialization


    public string sessionInfo;

    public string device;

    public string playerID;

    public long sessionLength;

    public long tutorialLvlCompletedTime;

    public long firstLvlCompletedTime;

    public long secondLvlCompletedTime;

    public int actionNumber;

    public int movementNumber;

    public long totalMovementTime;

    private bool finalSend;

    private Stopwatch measurer;

    void Start ()
    {
        playerID = "";
        tutorialLvlCompletedTime = -1;
        firstLvlCompletedTime = -1;
        secondLvlCompletedTime = -1;
        sessionInfo = System.DateTime.Now.ToString("yyy/MM/dd HH:mm:ss");
        measurer = new Stopwatch();
        measurer.Start();
    }


    public void sendDetail(string action, string efficiency, long duration = -1)
    {
        string timeStamp = System.DateTime.Now.ToString("yyy/MM/dd HH:mm:ss");
        if (photonView.isMine)
        {
            StartCoroutine(PostInfoDetail(action, timeStamp, efficiency, duration));
        }


    }

    public void send()
    {
        finalSend = true;
        if (photonView.isMine)
        {
            StartCoroutine(PostInfo());
        }
    }

    IEnumerator PostInfo()
    {
        WWWForm wForm = new WWWForm();
        wForm.AddField("entry.1421169018", playerID + sessionInfo);
        wForm.AddField("entry.646309232", device);
        wForm.AddField("entry.1400468149", sessionLength + "");
        wForm.AddField("entry.1140587957", tutorialLvlCompletedTime + "");
        wForm.AddField("entry.125922159", firstLvlCompletedTime + "");
        wForm.AddField("entry.239949461", secondLvlCompletedTime + "");
        wForm.AddField("entry.1992437386", actionNumber + "");
        wForm.AddField("entry.1612810784", movementNumber + "");
        wForm.AddField("entry.313368303", totalMovementTime + "");
        byte[] data = wForm.data;
        WWW urlPag = new WWW(URLGeneral, data);
        yield return urlPag;
    }

    IEnumerator PostInfoDetail(string action, string timeStamp, string efficiency, long duration)
    {
        WWWForm wForm = new WWWForm();
        wForm.AddField("entry.627883715", playerID + sessionInfo);
        wForm.AddField("entry.1114433300", device);
        wForm.AddField("entry.685076175", action);
        wForm.AddField("entry.1473852214", efficiency);
        wForm.AddField("entry.1003608839", timeStamp );
        wForm.AddField("entry.2090614852", duration + "");
        byte[] data = wForm.data;
        WWW urlPag = new WWW(URLDetalle, data);
        yield return urlPag;
    }

    public void levelCompleted(int level)
    {
        if(level == 0 && tutorialLvlCompletedTime <= 0)
        {
            measurer.Stop();
            tutorialLvlCompletedTime = measurer.ElapsedMilliseconds;
            measurer.Reset();
            measurer.Start();
        }
        else if (level == 1 && firstLvlCompletedTime <=0)
        {
            measurer.Stop();
            firstLvlCompletedTime = measurer.ElapsedMilliseconds;
            measurer.Reset();
            measurer.Start();
        }
        else if (level == 2 && secondLvlCompletedTime <= 0 )
        {
            measurer.Stop();
            secondLvlCompletedTime = measurer.ElapsedMilliseconds;
            measurer.Reset();
            measurer.Start();
            sessionLength = tutorialLvlCompletedTime + firstLvlCompletedTime + secondLvlCompletedTime;
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if(Time.time > 1800f && finalSend == false )
        {
            send();
            finalSend = true;
        }
        if(Input.GetKeyDown(KeyCode.F10))
        {
            finalSend = true;
            send();
        }
	}

    private void OnApplicationPause()
    {
        if(finalSend == false)
        {
            send();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
       // throw new NotImplementedException();
    }
}
