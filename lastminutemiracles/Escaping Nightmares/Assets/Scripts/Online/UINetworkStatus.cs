﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINetworkStatus : MonoBehaviour
{
    //---------------------------------------------
    // Public Variables
    //--------------------------------------------

    [Tooltip("Camera that will show the connection process")]
    public GameObject temporalCamera;

    [Tooltip("Array of player texts names that will say if is online or not")]
    public Text[] pTexts;

    [Tooltip("ID of the current Player")]
    public Text playerIDText;

    [Tooltip("Text that says the current connection status")]
    public Text connectionStatusText;


    //---------------------------------------------
    // Private Variables
    //--------------------------------------------

    [Tooltip("Id ")]
    private int currentPlayer;

    //---------------------------------------------
    // Methods
    //--------------------------------------------

    public void connectUI()
    {
        connectionStatusText.text = "Connecting...";
    }

    public void disconnectUI()
    {
        connectionStatusText.text = "...";
        pTexts[currentPlayer].text = "Offline";
        currentPlayer = -1;
    }

    public void JoinedRoomUI(bool isHost)
    {

        connectionStatusText.text = "Conectado al Servidor";
        temporalCamera.SetActive(false);
        this.gameObject.SetActive(true);
        //this.gameObject.transform.parent.GetChild(1).transform.gameObject.SetActive(true);
    }

    public void setPlayerName(string newName)
    {
        char[] charsName = newName.ToCharArray();
        string currentPlayerString = charsName[newName.Length-1]+"";
        currentPlayer = Int32.Parse(currentPlayerString);
       // Debug.Log("Curren Player is: " + currentPlayer);
        if(playerIDText != null)
        {
            playerIDText.text = newName;
        }
    }

    public void setPlayerStatusTexts(int index, string pStatus)
    {
        if(pTexts[index] != null)
        {
            pTexts[index].text = pStatus;
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
