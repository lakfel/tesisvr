﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetPlayerOnlineInfo : Photon.PunBehaviour
{
    public bool isCamera;
	// Use this for initialization
	void Awake ()
    {
        if(GameObject.Find("IsTesting") == null)
        {
            string newName = photonView.owner.NickName;
            if (isCamera)
            {
                string[] nameArray = newName.Split(' ');
                newName = "Camera " + nameArray[1];
            }
            this.gameObject.name = newName;
        }        
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
