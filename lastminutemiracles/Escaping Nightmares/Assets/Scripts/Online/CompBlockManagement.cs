﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompBlockManagement : Photon.PunBehaviour
{
    public Camera[] mainCameras;

    public MonoBehaviour[] scripts;

    public AudioListener audioListener;

    public GameObject[] personalGameObjects;

    public GameObject[] objectsToActivate;

	// Use this for initialization
	void Start ()
    {
        if(GameObject.Find("IsTesting") == null)
        {
            bool isLocalPlayer = photonView.isMine;
            if (mainCameras != null)
            {
                foreach (Camera temp in mainCameras)
                {
                    temp.enabled = isLocalPlayer;
                }
            }

            if (scripts != null)
            {
                foreach (MonoBehaviour temp in scripts)
                {
                    if (temp != null)
                    {
                        temp.enabled = isLocalPlayer;
                    }
                }
            }

            if (personalGameObjects != null)
            {
                foreach (GameObject temp in personalGameObjects)
                {
                    if (temp != null)
                    {
                        temp.SetActive(isLocalPlayer);
                    }
                }
            }

            if (audioListener != null)
            {
                audioListener.enabled = isLocalPlayer;
            }

            if (objectsToActivate != null)
            {
                foreach (GameObject temp in objectsToActivate)
                {
                    temp.SetActive(true);
                }
            }
        }        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
