﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LvlManager : Photon.PunBehaviour, IPunObservable
{
    //--------------------------------------
    // Public Variables
    //--------------------------------------

    public enum Platform { OculusTouch, GearVR, HTCVive };

    static public LvlManager Instance;

    // The order of the array is [0] = OculusTouch, [1] = GearVR, [2] = HTCVive
    [Tooltip("The prefab array of the different gameobjects necessary to summon a player")]
    public GameObject[] oculusTouchPlayer;

    [Tooltip("The prefab array of the different gameobjects necessary to summon a player")]
    public GameObject[] gearVRPlayer;

    [Tooltip("The prefab array of the different gameobjects necessary to summon a player")]
    public GameObject[] htcVivePlayer;

    [Tooltip("Spawn Locations for the players")]
    public Transform[] spawnLocations;

    // Array of the players
    public PlayerInteraction[] playersInteractions;


    //--------------------------------------
    // Private Variables
    //--------------------------------------

    private Platform currentPlatform;

    void Awake()
    {

    }
    // Use this for initialization
    void Start()
    {
        
    }

    public void spawnConnectedPlayer()
    {
        int platformSelected = PlayerPrefs.GetInt("Platform");
        currentPlatform = (Platform)platformSelected;
        //Debug.Log("The platform selected is: " + currentPlatform.ToString());
        Instance = this;

        if (PlayerInteraction.LocalPlayerInstance == null)
        {
            PhotonPlayer actualPlayer = PhotonNetwork.player;
            //Debug.Log("Nickname of the local is " + actualPlayer.NickName);
            spawnPlayer(actualPlayer.NickName, currentPlatform);
        }
        else
        {
            //Debug.Log("Ignoring scene load for " + SceneManager.GetActiveScene().name);
        }
    }

    public void spawnPlayer(string playerNickname, Platform destPlatform)
    {
        char[] nickNamechars = playerNickname.ToCharArray();
        int playerId = Int32.Parse(nickNamechars[playerNickname.Length - 1] + "");
        Transform spawnPosition = spawnLocations[playerId];
        GameObject.Find("/TutorialIslandP" + (playerId + 1) + "/Deco/Door/TutorialStatus/TutorialCanvas").transform.gameObject.SetActive(true);
        Vector3 neoSpawn = spawnPosition.position;
        GameObject[] playerArray = null;
        string[] contentNames = null;
        switch (destPlatform)
        {
            case Platform.OculusTouch:
                playerArray = oculusTouchPlayer;
                contentNames = new string[playerArray.Length];
                contentNames[0] = "OculusTouchCamera";
                contentNames[1] = "OTouchPlayer";
                break;
            case Platform.GearVR:
                playerArray = gearVRPlayer;
                contentNames = new string[playerArray.Length];
                contentNames[0] = "GRCamera";
                contentNames[1] = "GRAvatar";
                break;
            case Platform.HTCVive:

                playerArray = htcVivePlayer;
                contentNames = new string[playerArray.Length];
                contentNames[0] = "HTCCamera";
                contentNames[1] = "HTCPlayer";
                break;
        }
        for(int i = 0; i < playerArray.Length; i++)
        {
            GameObject tempPlayerContent = playerArray[i];
            GameObject newContent = PhotonNetwork.Instantiate(contentNames[i], spawnPosition.position, spawnPosition.rotation, 0);
            if(i == 0)
            {
                //GameObject interactionPart = newContent.GetComponentInChildren<OculusTouchInteraction>().gameObject;
                //interactionPart.GetComponent<PhotonView>().RPC("setPlayerInteraction", PhotonTargets.All, playerId);
            }
        }

        //Debug.Log("Player to be instantiated is: " + newPlayer.name);
        //Debug.Log("INFO IMPORTANTE : InstantiateOnNetwork is: " + PhotonNetwork.InstantiateInRoomOnly + " , inRoom: " + PhotonNetwork.inRoom);
    }

    /// <summary>
    /// Called when the local player left the room. We need to load the launcher scene.
    /// </summary>
    public override void OnLeftRoom()
    {
        //SceneManager.LoadScene(0);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    public Platform getCurrentPlatform()
    {
        return currentPlatform;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }


}
