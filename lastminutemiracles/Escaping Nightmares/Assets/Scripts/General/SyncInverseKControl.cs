﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SyncInverseKControl : Photon.PunBehaviour
{
    //---------------------------------------
    // Public Variables
    //---------------------------------------
    // Current part that will be attached to the IK Control in the avatar
    public string currentPart;

    public GameObject parent;

    //---------------------------------------
    // Methods
    //---------------------------------------

    // Use this for initialization
    void Start()
    {
        if (GameObject.Find("IsTesting") == null)
        {
            GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
            string[] nameSplit = parent.name.Split(' ');
            string number = nameSplit[1];
            foreach (GameObject temp in array)
            {
                if (temp.name.Contains("Player") && temp.name.Contains(number))
                {
                    IKControl ikControl = temp.GetComponent<IKControl>();
                    if (ikControl != null)
                    {
                        //temp.GetPhotonView().RPC("setPart", PhotonTargets.All, currentPart, this);
                        //ikControl.setPart(currentPart, transform);
                        break;
                    }
                }
            }
        }

    }
}
