﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUIHandler : MonoBehaviour
{
    public NetworkManager networkManager;
    public GameObject[] modifiableObjectsGear;
    public GameObject[] modifiableObjectsVive;

    // 0: PC, 1: OculusTouch, 2: GearVR, 3: HTCVive
    public GameObject[] platformCanvases;

    public void setCanvas(LvlManager.Platform currentPlatform)
    {
        int thePlatform = (int)currentPlatform;
        for (int i = 0; i < platformCanvases.Length; i++)
        {
            GameObject temp = platformCanvases[i];
            if (i == thePlatform)
            {
                temp.SetActive(true);
            }
            else
            {
                temp.SetActive(false);
            }
        }
    }
    private void Start()
    {
        
        
    }

    // Use this for initialization
    void Awake()
    {
        transform.GetChild(0).transform.gameObject.SetActive(true);
        setCanvas(networkManager.platform);
    }
}
