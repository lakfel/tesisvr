﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorMechanism : Photon.PunBehaviour, IPunObservable
{
    public int totalPuzzles;

    public Animator doorAnimator;

    private int expectedPuzzlesSolved;

    private int solvedPuzzles;

    // Use this for initialization
    void Start()
    {
        expectedPuzzlesSolved = totalPuzzles;
        solvedPuzzles = 0;
    }

    [PunRPC]
    public void checkIfSolved()
    {
        Debug.Log("Door: CheckinSolve");
        if (solvedPuzzles == expectedPuzzlesSolved)
        {
            doorAnimator.SetTrigger("OpenDoor");
        }
    }

    public void increaseSolvedPuzzles()
    {
        Debug.Log("Door: Increasing");
        solvedPuzzles++;
        if(PhotonNetwork.connectionState == ConnectionState.Connected && photonView != null)
        {
            photonView.RPC("checkIfSolved", PhotonTargets.All);
        }
        else
        {
            checkIfSolved();
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
