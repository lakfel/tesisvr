﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinnersAnnouncement : MonoBehaviour
{
    public GameObject[] awesomeLights;

    public float totalTime;

    private float startTime;

    private float endtime;

   
	// Use this for initialization
	void Start ()
    {
        startTime = Time.time;
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "HTCPlayer" || other.tag == "GearPlayer")
        {
            endtime = Time.time;

            totalTime = endtime - startTime;
            
            foreach(GameObject temp in awesomeLights)
            {
                temp.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update () {
		
	}
}
