﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleTwoObjects : Photon.PunBehaviour, IPunObservable
{
    public GameObject expectedObject;

    public DoorMechanism associatedDoor;

    public SphereCollider areaCollider;

    public GameObject spotlight;

    // Use this for initialization
    void Start()
    {
    }

    public bool checkPuzzle(GameObject receivedObject)
    {
        bool answer = false;
        if (expectedObject == receivedObject)
        {
            answer = true;
        }
        return answer;
    }

    [PunRPC]
    public void solvePuzzle()
    {
        Debug.Log("Puzzle: Resolciendo Puzzle");
        associatedDoor.increaseSolvedPuzzles();
        this.enabled = false;
        areaCollider.enabled = false;
        spotlight.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new NotImplementedException();
    }
}
